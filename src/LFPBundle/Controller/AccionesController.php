<?php

namespace LFPBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;

use LFPBundle\Entity\Jugadores;
use LFPBundle\Form\JugadoresType;
use LFPBundle\Entity\Club;
use LFPBundle\Form\ClubType;

class AccionesController extends Controller
{
	private $session;
	
	public function __construct()
	{
		$this->session=new Session();
	}
	
	public function indexAction()
	{
		return $this->render('LFPBundle:Default:index.html.twig');
	}

	public function anadirClubAction(Request $request)
	{
		$club = new Club();
		$form = $this->createForm(ClubType::class,$club);

		$form->handleRequest($request);
		$status = "";

		if($form->isSubmitted())
		{
			if($form->isValid())
			{
				$em = $this->getDoctrine()->getManager();

				$em->persist($club);
				$flush = $em->flush();

				if($flush==null)
				{
					$this->session->getFlashBag()->add("success", "El Club se ha creado correctamente !!");
				}
				else
				{
					$this->session->getFlashBag()->add("danger", "Error al Añadir el Club");
				}
			}
			else
			{
				for($i=0;(count($errors)-1)>=$i;$i++)
				{
					$this->session->getFlashBag()->add("danger", $errors[$i]->getMessage());
				}
			}
			return $this->redirectToRoute("lfp_listar_club");
		}

		return $this->render("LFPBundle:Default:anadirClub.html.twig",array(
			"form" => $form->createView()
		));
	}
	
	public function editarClubAction(Request $request, $id)
	{
		$em = $this->getDoctrine()->getManager();
		$club_repo = $em->getRepository("LFPBundle:Club");
		$club = $club_repo->find($id);
		
		$form = $this->createForm(ClubType::class,$club);
		
		$form->handleRequest($request);
		
		if($form->isSubmitted())
		{
			if($form->isValid())
			{
				$em->persist($club);
				$flush = $em->flush();
				
				if($flush==null)
				{
					$this->session->getFlashBag()->add("success", "El Club se ha modificado correctamente !!");
				}
				else
				{
					$this->session->getFlashBag()->add("danger", "Error al modificar el Club");
				}
				
			}
			else
			{
				$validator = $this->get('validator');
				$errors = $validator->validate($jug);

				for($i=0;(count($errors)-1)>=$i;$i++)
				{
					$this->session->getFlashBag()->add("danger", $errors[$i]->getMessage());
				}
			}
			
			return $this->redirectToRoute("lfp_listar_club");
		}
		
		return $this->render("LFPBundle:Default:editarClub.html.twig",array(
			"form" => $form->createView()
		));
	}
	
	public function eliminarClubAction($id)
	{
		$serv = $this->get("app.acciones");
		$res = $serv->borradoLogico($id);
		
		if($res==true)
		{
			$this->session->getFlashBag()->add("success", "El Club se ha eliminado correctamente !!");
		}
		else
		{
			$this->session->getFlashBag()->add("danger", "Error al eliminar el Club");
		}
		
		return $this->redirectToRoute("lfp_listar_club");
	}

	public function listarClubAction()
	{
		$em = $this->getDoctrine()->getManager();
		$club_repo = $em->getRepository("LFPBundle:Club");
		$club = $club_repo->findAll();
		$club = $club_repo->findBy(array('borrado' => 0));


		return $this->render("LFPBundle:Default:listarClub.html.twig",array(
			"clubes" => $club
		));
		
		return $this->render('LFPBundle:Default:listarClub.html.twig');
	}

	public function anadirJugadorAction(Request $request)
	{
		$jug = new Jugadores;
		$form = $this->createForm(JugadoresType::class,$jug);

		$form->handleRequest($request);
		$status = "";

		if($form->isSubmitted())
		{
			if($form->isValid())
			{
				$em = $this->getDoctrine()->getManager();

				$em->persist($jug);
				$flush = $em->flush();

				if($flush==null)
				{
					$this->session->getFlashBag()->add("success", "El jugador se ha creado correctamente !!");
				}
				else
				{
					$this->session->getFlashBag()->add("danger", "Error al Añadir el Jugador");
				}
			}
			else
			{
				for($i=0;(count($errors)-1)>=$i;$i++)
				{
					$this->session->getFlashBag()->add("danger", $errors[$i]->getMessage());
				}
			}
			return $this->redirectToRoute("lfp_anadir_jugador");
		}

		return $this->render("LFPBundle:Default:anadirJugador.html.twig",array(
			"form" => $form->createView()
		));
		
	}
}
